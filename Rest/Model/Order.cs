namespace Rest.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Order")]
    public partial class Order
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Order()
        {
            OrderString = new HashSet<OrderString>();
        }

        public int OrderID { get; set; }

        [Column(TypeName = "date")]
        public DateTime Date { get; set; }

        public bool? Status_paid { get; set; }

        public int TableFK { get; set; }

        public int UserFK { get; set; }

        [Column(TypeName = "money")]
        public decimal Total { get; set; }

        public virtual Table Table { get; set; }

        public virtual User User { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderString> OrderString { get; set; }
    }
}
