namespace Rest.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OrderString")]
    public partial class OrderString
    {
        public int OrderStringID { get; set; }

        public int Quantity { get; set; }

        [Column(TypeName = "money")]
        public decimal Price { get; set; }

        public bool Serving_way { get; set; }

        [StringLength(50)]
        public string Comment { get; set; }

        public int OrderFK { get; set; }

        public int StatusFK { get; set; }

        public int DishFK { get; set; }

        public int? UserFK { get; set; }

        public virtual Dish Dish { get; set; }

        public virtual Order Order { get; set; }

        public virtual Status Status { get; set; }

        public virtual User User { get; set; }
    }
}
