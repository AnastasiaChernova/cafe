﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Rest.Model;

namespace Rest.ViewModel
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        
        Model1 db = new Model1();

        public Table SelectedTable { get; set; }
        public List<Table> Tables { get; set; }
        public List<Order> Orders { get; set; }
        private decimal total { get; set; }
        public decimal Total
        {
            get { return total; }
            set
            {
                total = value;
                OnPropertyChanged("Total");
            }
        }
        private  ObservableCollection<OrderString> strings { get; set; }
        public ObservableCollection<OrderString> Strings
        {
            get { return strings; }
            set
            {
                strings = value;
                OnPropertyChanged("Strings");
            }
        }
        private RelayCommand delCommand;
        public RelayCommand DelCommand
        {
            get 
            {
                return delCommand ??
                   (delCommand = new RelayCommand(obj =>
                   {
                       OrderString orderString = (OrderString)obj;
                       db.OrderString.Remove(orderString);
                       List<Order> oList = db.Order.Where(i => i.TableFK == SelectedTable.TableID && i.Status_paid == false).ToList();
                       if (oList.Count > 0)
                       {
                           Order o = oList.First();
                           
                           o.Total -= orderString.Quantity * orderString.Price;
                           Total = o.Total;
                           db.SaveChanges();

                           List<OrderString> orderStrings = db.OrderString.Where(i => i.OrderFK == o.OrderID).ToList();
                           Strings = new ObservableCollection<OrderString>();
                           foreach (OrderString os in orderStrings)
                               Strings.Add(os);
                       }
                   }));
            }
        }

        private RelayCommand selectedCommand;

        public RelayCommand SelectedCommand
        {
            get
            {
                return selectedCommand ??
                    (selectedCommand = new RelayCommand(obj =>
                    {
                        Table t = (Table)obj;
                        List<Order> oList = db.Order.Where(i => i.TableFK == t.TableID && i.Status_paid == false).ToList();
                        if (oList.Count > 0)
                        {
                            Order o = oList.First();
                            List<OrderString> orderStrings = db.OrderString.Where(i => i.OrderFK == o.OrderID).ToList();
                            foreach (OrderString os in orderStrings)
                                Strings.Add(os);
                            Total = o.Total;
                        }
                        else
                        {
                            Strings.Clear();
                            Total = 0;
                        }
                    },
                    (obj) => (obj != null)));

            }
        }
        public MainWindowViewModel()
        {
            Tables = db.Table.ToList();
            Orders = db.Order.ToList();
            Strings = new ObservableCollection<OrderString>();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        public void AddString(DishViewModel d)
        {
            List<Order> oList = db.Order.Where(i => i.TableFK == SelectedTable.TableID && i.Status_paid == false).ToList();
            if (oList.Count > 0)
            {
                Order o = oList.First();
                OrderString ostring = new OrderString
                {
                    Quantity = 1,
                    Price = d.Price,
                    Serving_way = true,
                    OrderFK = o.OrderID,
                    StatusFK = 2,
                    DishFK = d.DishID,
                    Dish = db.Dish.Find(d.DishID)
                    
                };
                ostring.Order = db.Order.Find(ostring.OrderFK);
                ostring.Status = db.Status.Find(ostring.StatusFK);
                db.OrderString.Add(ostring);
                o.Total += ostring.Quantity * ostring.Price;
                db.SaveChanges();
                List<OrderString> orderStrings = db.OrderString.Where(i => i.OrderFK == o.OrderID).ToList();
                foreach (OrderString os in orderStrings)
                    Strings.Add(os);
                Total = o.Total;
            }

        }
    }

}
