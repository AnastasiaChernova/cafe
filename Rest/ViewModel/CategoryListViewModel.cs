﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Navigation;
using Rest.Model;

namespace Rest.ViewModel
{
    class CategoryListViewModel
    {
        NavigationService navigation;
        public List<Category> categories { get; set; }
        private MainWindowViewModel MainW;

        public CategoryListViewModel(NavigationService navigation, MainWindowViewModel MainW)
        {
            Model1 model = new Model1();
            this.MainW = MainW;
            categories = model.Category.ToList();
            this.navigation = navigation;
        }

        private ICommand _dishCommand;

        public ICommand DishCommand
        {
            get
            {
                if (_dishCommand == null)
                    _dishCommand = new RelayCommand(obj =>
                    {
                        navigation.Navigate(new View.DishList(new DishListViewModel( (Category)obj, MainW)));
                    });
                return _dishCommand;
            }
            set { _dishCommand = value; }
        }
    }
}
