﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Rest.Model;


namespace Rest.ViewModel
{
    class DishListViewModel
    {
        public List<DishViewModel> dishes { get; set; }
        private MainWindowViewModel MainW;
        Model1 model;
        public DishListViewModel(Category cat, MainWindowViewModel MainW)
        {
            this.MainW = MainW;
            model = new Model1();
            List<Dish> di = model.Dish.ToList().Where(i => i.CategoryFK == cat.CategoryID).ToList();
            dishes = new List<DishViewModel>();
            foreach(Dish d in di)
                dishes.Add(new DishViewModel(d));
        }

        private ICommand _dishCommand;

        public ICommand DishCommand
        {
            get
            {
                if (_dishCommand == null)
                    _dishCommand = new RelayCommand(obj =>
                    {
                        MainW.AddString((DishViewModel)obj);

                    });
                return _dishCommand;
            }
            set { _dishCommand = value; }
        }
    }
}
