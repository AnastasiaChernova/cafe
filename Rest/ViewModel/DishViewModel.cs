﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rest.Model;


namespace Rest.ViewModel
{
     public class DishViewModel
    {
        public int DishID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public bool Available { get; set; }
        public string Image { get; set; }

        public DishViewModel(Dish d)
        {
            DishID = d.DishID;
            Name = d.Name;
            Description = d.Description;
            Price = d.Price;
            Available = d.Available;
            Image = d.Image;
        }
    }
}
